# nasdaq_test

Simple group of classes mimicking a drone. Uncomment scenarios in the main `FlyDrone.java` file to see different scenarios in action.

1. Drone flies around and lands normally
2. Drone flies around and an engine fails, prompting an emergency landing
3. Drone is hit and corrects itself while flying around
4. Drone detects a faulty engine and cannot take off

To compile

```
$ javac nasdaq_test/*.java
$ java nasdaq_test.FlyDrone
```