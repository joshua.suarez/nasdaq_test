package nasdaq_test;

import java.util.*;

public class Drone{
    private int status = 0; //0 for off, 1 for hovering, 2 for moving
    private Gyroscope gyro = new Gyroscope();
	private OrientationSensor sensor = new OrientationSensor();
    private List<Engine> engines = new ArrayList<Engine>();

	//Add the number of engines passed in. By default these are powered on to 0%
    public Drone(int numEngines){
		System.out.println("Booting Drone...");
		for(int i = 0; i < numEngines; i++){
			engines.add(new Engine(i+1));
		}
		System.out.println(numEngines + " engines initialized.");
    }

    public String getStatus(){
		String[] statusVals = {"Off", "Hovering", "Moving"};
		return statusVals[this.status];
    }
	
	public void printStatus(){
		System.out.println("Current Status: " + this.getStatus());
	}

    public int getGyroVelocityX(){
		return this.gyro.getVelocityX();
    }
	
	public int getGyroVelocityY(){
		return this.gyro.getVelocityY();
    }
	
	public int getGyroVelocityZ(){
		return this.gyro.getVelocityZ();
    }
	
	//For simplicity sake, hovering power will be 50% of engine thrust
	public void take_off(){
		if(this.status != 0){
			System.out.println("This drone is already flying!");
			return;
		}
			
		System.out.println("Initiating liftoff...");
		this.setEnginePower(50);
		
		this.status = 1;
	}
	
	
	public void land(){
		if(this.status == 0){
			System.out.println("This drone off!");
			return;
		}
		
		this.stabilize();
			
		System.out.println("Initiating landing...");
		for(int i = 40; i >= 0; i -= 10)
			this.setEnginePower(i);
		
		System.out.println("Drone has landed");
		
		this.status = 0;
	}
	
	//Sets all movement vectors to 0 and returns the drone to "hover" state
	public void stabilize(){
		System.out.println("Stabilizing...");
		this.move(0,0,0,0,0);
		this.setEnginePower(50);
		this.status = 1;
	}
	
	
	public void move_forward(){
		this.stabilize();
		System.out.println("Moving forward...");
		
		//Set the pitch forward 15 degrees and the x-gyro 1 degree per second
		this.move(1,0,0,15,0);
		this.status = 2;
	}
	
	public void move_backward(){
		this.stabilize();
		System.out.println("Moving backward...");
		
		//Set the pitch backward 15 degrees and the x-gyro -1 degree per second
		this.move(-1,0,0,-15,0);
		this.status = 2;
	}
	
	public void move_left(){
		this.stabilize();
		System.out.println("Moving left...");
		
		//Set the roll -15 degrees and the y-gyro -1 degree per second
		this.move(0,-1,0,0,-15);
		this.status = 2;
	}
	
	public void move_right(){
		this.stabilize();
		System.out.println("Moving right...");
		
		//Set the roll 15 degrees and the y-gyro 1 degree per second
		this.move(0,1,0,0,15);
		this.status = 2;
	}
	
	public void move_up(){
		this.stabilize();
		System.out.println("Moving up...");
		
		//Set the pitch forward 15 degrees and the x-gyro 1 degree per second
		this.setEnginePower(100);
		this.status = 2;
	}
	
	public void move_down(){
		this.stabilize();
		System.out.println("Moving down...");
		
		//Set the pitch forward 15 degrees and the x-gyro 1 degree per second
		this.setEnginePower(25);
		this.status = 2;
	}
	
	
	//Shut off any of the engines. Since the IDs are 1 based, take away 1 from
	//n to access the correct one in the list
	public void shutOffEngine(int n){
		n = n - 1;
		if(n >= 0 && n < this.engines.size())
			this.engines.get(n).powerOff();
	}
	
	//Assign random values to the gyro and orientation to simulate getting hit
	public void hitDrone(){
		this.move(this.randomInt(), this.randomInt(), this.randomInt(), this.randomInt(), this.randomInt());
		System.out.println("Hit detected!");
		this.stabilize();
	}
	
	


	//-----------------------------------------------------------------------------------
	//Private methods
	
	//function that will handle all movements, called from other public functions
	private void move(int x, int y, int z, int pitch, int roll){
		this.gyro.setVelocityX(x);
		this.gyro.setVelocityY(y);
		this.gyro.setVelocityZ(z);

		this.sensor.setPitch(pitch);
		this.sensor.setRoll(roll);
	}
	
	private void setEnginePower(int power){
		try{
			for( Engine e : engines){
				e.setPower(power);
			}	
			
		//If an engine is off mid flight, initiate the emergency landing
		} catch (EngineFailureException e){
			System.out.println(e.toString());
			
			if(this.status != 0){
				System.out.println("SOS: Making emergency landing...");
				this.move(0,0,0,0,0);
				this.status = 1;
				
				//shut down the remaing engines slowly for a gentle landing
				//Not the most elegant, doesn't take into account multiple engines
				//failing during the landing
				for(int i = 40; i >= 0; i -= 10)
					for( Engine eng : engines)
						if(eng.getStatus())
							try{ 
								eng.setPower(i); 
							} catch(Exception exp){}
						
				this.status = 0;
				System.out.println("Drone has landed...");
			}
			else if(this.status == 0){
				System.out.println("SOS: Cannot take off due to engine failure!");
			}
			System.exit(1);
		}
	}
	
	//get a random int between -180 and 180
	private int randomInt(){
		return (int)(Math.random() * (180 + 180) - 180);
	}
}
