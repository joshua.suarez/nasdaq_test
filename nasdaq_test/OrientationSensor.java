package nasdaq_test;

public class OrientationSensor{
	private int pitch;
	private int roll;
	
	public OrientationSensor(){
		this.pitch = 0;
		this.roll = 0;
	}
	
	public void setPitch(int pitch){
		this.pitch = pitch;
	}
	
	public void setRoll(int roll){
		this.roll = roll;
	}
	
	public int getPitch(){
		return this.pitch;
	}
	
	public int getRoll(){
		return this.roll;
	}
}