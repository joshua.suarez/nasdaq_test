package nasdaq_test;

class Engine{
	private int id;
    private int power = 0;
    private boolean status = false; //true for on, false for off
	
	public Engine(int id){
		this.id = id;
		this.status = true;
		System.out.println("Engine " + this.id + " powering on...");
	}

    public void setPower(int newPowerLevel) throws EngineFailureException{
		//Can't set the power if the engine is off
		if(!this.status)
			throw new EngineFailureException("Engine " + this.id + " is off!");
		
		if(newPowerLevel > 100){
			newPowerLevel = 100;
		}
		else if(newPowerLevel < 0){
			newPowerLevel = 0;
		}
		
		this.power = newPowerLevel;
		System.out.println("Power for Engine " + this.id + " set to " + this.power + "%");
    }
	
	public boolean getStatus(){
		return this.status;
	}
	
	public void powerOff(){
		this.status = false;
		System.out.println("Engine " + this.id + " powering off...");
	}
}
