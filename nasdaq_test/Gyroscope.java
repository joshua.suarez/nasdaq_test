package nasdaq_test;

/*
 * measures the velocity (rate of change) of each vector in degrees per second
*/
public class Gyroscope{
    private int vect_x = 0;
    private int vect_y = 0;
    private int vect_z = 0;

    public void setVelocityX(int x){
		this.vect_x = x;
    }
	
	public void setVelocityY(int Y){
		this.vect_y = Y;
    }
	
	public void setVelocityZ(int Z){
		this.vect_z = Z;
    }
    
    public int getVelocityX(){
		return this.vect_x;
    }
	
	public int getVelocityY(){
		return this.vect_y;
    }
	
	public int getVelocityZ(){
		return this.vect_z;
    }
}
