package nasdaq_test;

public class FlyDrone {
    public static void main(String[] args){
		//Scenario 1, just fly around and land
		//Initialize with 4 engines
		System.out.println("------------------SCENARIO 1--------------------");
		Drone d1 = new Drone(4);
		d1.printStatus();
		d1.take_off();
		d1.printStatus();
		d1.move_forward();
		d1.move_up();
		d1.move_right();
		d1.printStatus();
		d1.move_forward();
		d1.land();
		d1.printStatus();
		
		
		// //Scenario 2, fly around and an engine fails
		// System.out.println("------------------SCENARIO 2--------------------");
		// Drone d2 = new Drone(4);
		// d2.printStatus();
		// d2.take_off();
		// d2.printStatus();
		// d2.move_forward();
		// d2.move_up();
		// d2.move_right();
		// d2.printStatus();
		// d2.shutOffEngine(3);
		// d2.move_forward();
		// d2.printStatus();
		
		// //Scenario 3, slap the drone so it corrects itself
		// System.out.println("------------------SCENARIO 3--------------------");
		// Drone d3 = new Drone(4);
		// d3.printStatus();
		// d3.take_off();
		// d3.printStatus();
		// d3.move_forward();
		// d3.move_up();
		// d3.move_right();
		// d3.hitDrone();
		// d3.printStatus();
		// d3.move_forward();
		// d3.printStatus();
		// d3.move_down();
		// d3.printStatus();
		// d3.land();
		// d3.printStatus();
		
		// //Scenario 4, shut off an engine before take off, drone will send sos signal
		// System.out.println("------------------SCENARIO 14--------------------");
		// Drone d4 = new Drone(4);
		// d4.printStatus();
		// d4.shutOffEngine(2);
		// d4.take_off();
		// d4.printStatus();
		// d4.move_forward();
		// d4.move_up();
		// d4.move_right();
		// d4.printStatus();
		// d4.move_forward();
		// d4.printStatus();
		// d4.move_down();
		// d4.printStatus();
		// d4.move_left();
		// d4.printStatus();
		// d4.land();
		// d4.printStatus();
    }
}
