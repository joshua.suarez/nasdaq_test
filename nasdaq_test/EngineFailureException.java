package nasdaq_test;

public class EngineFailureException extends Exception {
    public EngineFailureException(String msg){
	super(msg);
    }

}
